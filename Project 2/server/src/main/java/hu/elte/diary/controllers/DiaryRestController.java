///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package hu.elte.diary.controllers;
//
///**
// *
// * @author SalmaMoustafa
// */
//public class DiaryRestController {
//    
//}
package hu.elte.diary.controllers;

import hu.elte.diary.entities.Diary;
import hu.elte.diary.repositories.DiaryRepository;
//import hu.elte.IssueTracker.repositories.LabelRepository;
//import hu.elte.IssueTracker.repositories.MessageRepository;
//import hu.elte.IssueTracker.repositories.UserRepository;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/diarys")
public class DiaryRestController {
    
    @Autowired
    private DiaryRepository diaryRepository;
    
//    @Autowired
//    private MessageRepository messageRepository;
//    
//    @Autowired
//    private LabelRepository labelRepository;
//    
//    @Autowired
//    private UserRepository userRepository;
    
    @GetMapping("")
    public ResponseEntity<Iterable<Diary>> getAll(Principal principal) {
        return ResponseEntity.ok(diaryRepository.findAll());
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Diary> get(@PathVariable Integer id) {
        Optional<Diary> diary = diaryRepository.findById(id);
        if (diary.isPresent()) {
            return ResponseEntity.ok(diary.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
    @PostMapping("")
    public ResponseEntity<Diary> post(@RequestBody Diary diary, Principal principal) {
        Diary savedDiary = diaryRepository.save(diary);
        return ResponseEntity.ok(savedDiary);
    }
    
    @PutMapping("/{id}")
    public ResponseEntity<Diary> update
            (@PathVariable Integer id,
             @RequestBody Diary diary) {
        Optional<Diary> oDiary = diaryRepository.findById(id);
        if (oDiary.isPresent()) {
            diary.setId(id);
            return ResponseEntity.ok(diaryRepository.save(diary));
        } else {
            return ResponseEntity.notFound().build();
        }
    }
            
//    @DeleteMapping("/{id}")
//    public ResponseEntity<Diary> delete
//            (@PathVariable Integer id) {
//        Optional<Diary> oDiary = diaryRepository.findById(id);
//        if (oIssue.isPresent()) {
//            issueRepository.deleteById(id);
//            return ResponseEntity.ok().build();
//        } else {
//            return ResponseEntity.notFound().build();
//        }
//    }
     
}
