///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package hu.elte.diary.dto;
//
///**
// *
// * @author SalmaMoustafa
// */
//public class DiaryListDTO {
//    
//}

package hu.elte.diary.dto;

import hu.elte.diary.entities.Diary;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class DiaryListDTO extends Diary {
    private long count;
    
    public DiaryListDTO(Integer id, String title, String text, LocalDateTime created_at, LocalDateTime updated_at, long count) {
        super(id, title, text, created_at, updated_at);
        this.count = count;
    }
}
