///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package hu.elte.diary.repositories;
//
///**
// *
// * @author SalmaMoustafa
// */
//public interface DiaryRepository {
//    
//}
package hu.elte.diary.repositories;

import hu.elte.diary.entities.Diary;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiaryRepository extends CrudRepository<Diary, Integer> {
//    public Iterable<Issue> findAllByUser(User user);
    
//    @Query("SELECT new hu.elte.IssueTracker.dtos.IssueListDTO(i.id, i.title, i.description, i.place, i.status, i.created_at, i.updated_at, count(m.id)) FROM Issue i LEFT JOIN i.messages m WHERE i.user = ?1 GROUP BY i")
//    public Iterable<Issue> findAllByUserWithMessageCount(User user);
//    
//    @Query("SELECT new hu.elte.IssueTracker.dtos.IssueListDTO(i.id, i.title, i.description, i.place, i.status, i.created_at, i.updated_at, count(m.id)) FROM Issue i LEFT JOIN i.messages m GROUP BY i")
//    List<IssueListDTO> findAllIssueWithMessageCount();
}
