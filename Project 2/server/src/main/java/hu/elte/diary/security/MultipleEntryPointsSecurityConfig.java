///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package hu.elte.diary.security;
//
///**
// *
// * @author SalmaMoustafa
// */
//public class MultipleEntryPointsSecurityConfig {
//    
//}
package hu.elte.diary.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class MultipleEntryPointsSecurityConfig {

  
    @Configuration
    @Order(1)
    public static class RestWebSecurityConfig extends WebSecurityConfigurerAdapter {

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.antMatcher("/api/**")
                .authorizeRequests()
                    .anyRequest().anonymous()
                    .and()
                .httpBasic()
                    .and()
                .csrf()
                    .disable();
        }

    }
    
    @Configuration
    @Order(2)
    public static class ApplWebSecurityConfig extends WebSecurityConfigurerAdapter {

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.antMatcher("/**")
                .authorizeRequests()
                    .antMatchers("/", "/h2/**", "/register").permitAll()
                    .anyRequest().authenticated()
                    .and()
                .csrf() // important!
                    .ignoringAntMatchers("/h2/**")
                    .and()
                .headers()
                    .frameOptions().disable(); // important!
        }

    }
    
}
