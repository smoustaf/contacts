// // import { Component, OnInit } from '@angular/core';

// // @Component({
// //   selector: 'app-diary-list',
// //   templateUrl: './diary-list.component.html',
// //   styleUrls: ['./diary-list.component.css']
// // })
// // export class DiaryListComponent implements OnInit {

// //   constructor() { }

// //   ngOnInit() {
// //   }

// // }
// import { Component, OnInit } from '@angular/core';
// import { Diary } from '../diary';
// import { FormGroup, FormBuilder } from '@angular/forms';

// @Component({
//   selector: 'app-diary-list',
//   templateUrl: './diary-list.component.html',
//   styleUrls: ['./diary-list.component.css']
// })
// export class DiaryListComponent implements OnInit {

//   diarys: Diary[] = [
//     { id: 1, title: 'diary1', text: 'desc1'},
//     { id: 2, title: 'diary2', text: 'desc2'},
//     { id: 3, title: 'diary3', text: 'desc3' },
//     { id: 4, title: 'diary4', text: 'desc4'},
//   ];
//   // filteredDiarys: Diary[] = diarys;
//   // filter = "ALL"
//   selectedDiary: Diary

//   constructor() {}

//   ngOnInit() {
//     // this.filterDiarys(this.filter);
//   }

//   // handleChange(status) {
//   //   this.filter = status
//   //   this.filterIssues(status)
//   // }

//   // filterDiarys(filter) {
//   //   this.filteredDiarys = this.diarys;
//   //   console.log("HERE");
//   // }

//   onFormSave(formData) {
//     Object.assign(this.selectedDiary, formData)
//     this.selectedDiary = null
//   }

// }


import { Component, OnInit } from '@angular/core';
import { Diary } from '../diary';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DiaryService } from '../diary.service';

@Component({
  selector: 'app-diary-list',
  templateUrl: './diary-list.component.html',
  styleUrls: ['./diary-list.component.css']
})
export class DiaryListComponent implements OnInit {

  diarys: Diary[];
  filteredDiarys: Diary[] = [];
  status = 'ALL';
  selectedDiary: Diary;

  constructor(private diaryService: DiaryService) {}

  async ngOnInit() {
    try {
      this.diarys = await this.diaryService.getDiarys();
      // this.filterDiarys(this.status);
    } catch (e) {
      console.log(e);
    }
  }

  // handleStatusChange(status: string) {
  //   // console.log(this.radioGroupForm.value.status);
  //   this.filterDiarys(status);
  // }

  filterDiarys(filter) {
    this.filteredDiarys = this.diarys;
  }

  handleFormSubmit(formData) {
    Object.assign(this.selectedDiary, formData);
    this.selectedDiary = null;
  }
}

