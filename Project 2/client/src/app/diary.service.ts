// import { Injectable } from '@angular/core';

// @Injectable({
//   providedIn: 'root'
// })
// export class DiaryService {

//   constructor() { }
// }

import { Injectable } from '@angular/core';
import { Diary } from './diary';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 
    'Content-Type': 'application/json',
    'X-Requested-With': 'XMLHttpRequest'
  })
};

@Injectable({
  providedIn: 'root'
})
export class DiaryService {

  private diaryUrl = '/api/diarys';

  constructor(
    private http: HttpClient
  ) { }

  getDiarys(): Promise<Diary[]> {
    return this.http.get<Diary[]>(this.diaryUrl, httpOptions).toPromise();
  }

  getDiary(id: number): Promise<Diary> {
    return this.http.get<Diary>(`${this.diaryUrl}/${id}`, httpOptions).toPromise();
  }

  addDiary(formData): Promise<Diary> {
    return this.http.post<Diary>(this.diaryUrl, formData, httpOptions).toPromise();
  }

  modifyDiary(id: number, formData): Promise<Diary> {
    return this.http.put<Diary>(`${this.diaryUrl}/${id}`, formData, httpOptions).toPromise();
  }

  // deleteDiary(id: number) {
  //   return this.http.delete(`${this.diaryUrl}/${id}`, httpOptions).toPromise();
  // }
}
