// import { Component, OnInit } from '@angular/core';
// import { Diary } from '../diary';

// @Component({
//   selector: 'app-diary-form',
//   templateUrl: './diary-form.component.html',
//   styleUrls: ['./diary-form.component.css']
// })
// export class DiaryFormComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }

// import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
// import { Diary} from '../diary';
// import { FormBuilder, Validators } from '@angular/forms';

// @Component({
//   selector: 'app-diary-form',
//   templateUrl: './diary-form.component.html',
//   styleUrls: ['./diary-form.component.css']
// })
// export class DiaryFormComponent implements OnInit, OnChanges {

//   @Input() diary: Diary
//   @Output() save = new EventEmitter()

//   constructor(
//     private fb: FormBuilder
//   ) { }

//   diaryForm = this.fb.group({
//     title: ['', [Validators.required]],
//     text: ['', [Validators.required]],
//   });

//   get title() { return this.diaryForm.get('title'); }
//   get text() { return this.diaryForm.get('text'); }


//   ngOnInit() {
//   }

//   ngOnChanges() {
//     this.diaryForm.patchValue(this.diary)
//   }

//   onSubmit() {
//     this.save.emit(this.diaryForm.value);
//   }

// }
import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Diary } from '../diary';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-diary-form',
  templateUrl: './diary-form.component.html',
  styleUrls: ['./diary-form.component.css']
})
export class DiaryFormComponent implements OnInit, OnChanges {

  @Input() diary: Diary;
  @Output() save = new EventEmitter<Diary>();

  form = this.fb.group({
    title: ['', [Validators.required]],
    text: [''],
  });

  get title() { return this.form.get('title'); }
  get text() { return this.form.get('text'); }

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
  }

  ngOnChanges() {
    this.form.patchValue(this.diary);
  }

  onSubmit() {
    // const emittedIssue = Object.assign(this.issue, this.form.value);
    this.save.emit(this.form.value);
  }

}
