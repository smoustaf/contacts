// import { NgModule } from '@angular/core';
// import { Routes, RouterModule } from '@angular/router';

// const routes: Routes = [];

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainPageComponent } from './main-page/main-page.component';
import { DiaryListComponent } from "./diary-list/diary-list.component";
import { DiaryFormComponent } from './diary-form/diary-form.component';
import { DiaryEditComponent } from './diary-edit/diary-edit.component';
import { AboutComponent } from './about/about.component';

const routes: Routes = [
  // {
  //   path: '',
  //   redirectTo: '/diarys',
  //   pathMatch: 'full'
  // },
  // {
  //   path: 'diarys',
  //   component: DiaryListComponent
  // },
  // {
  //   path: 'diarys/new',
  //   component: DiaryFormComponent
  // },
  // {
  //   path: 'diarys/about',
  //   component: AboutComponent
  // },
  // {
  //   path: 'diarys/edit',
  //   component: DiaryEditComponent
  // },

  {
    path: '',
    component: MainPageComponent
  },
  {
    path: 'diarys',
    component: DiaryListComponent
  },
  // {
  //   path: 'diarys/new',
  //   component: DiaryEditComponent
  // },
  {
    path: 'diarys/new',
    component: DiaryFormComponent
  },
  {
    path: 'diarys/about',
    component: AboutComponent
  },
  {
    path: 'diarys/:id/edit',
    component: DiaryEditComponent
  },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes)  ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }