// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-diary-edit',
//   templateUrl: './diary-edit.component.html',
//   styleUrls: ['./diary-edit.component.css']
// })
// export class DiaryEditComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }

import { Component, OnInit } from '@angular/core';
import { Diary } from '../diary';
import { DiaryService } from '../diary.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-diary-edit',
  templateUrl: './diary-edit.component.html',
  styleUrls: ['./diary-edit.component.css']
})
export class DiaryEditComponent implements OnInit {

  diary: Diary = new Diary();
  id: number = null;

  constructor(
    private diaryService: DiaryService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router
  ) { }

  async ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.id = +id;
      this.diary = await this.diaryService.getDiary(this.id);
    }
  }

  async handleSave(formData) {
    if (this.id) {
      await this.diaryService.modifyDiary(this.id, formData);
      this.location.back();
    } else {
      await this.diaryService.addDiary(formData);
      this.router.navigate(['/diaryss']);
    }
  }
}
