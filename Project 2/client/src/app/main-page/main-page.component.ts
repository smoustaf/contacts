import { Component, OnInit } from '@angular/core';
import { DiaryService } from '../diary.service';
import { Diary } from '../diary';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  diarys: Diary[];
  selectedDiary: Diary;

  constructor(private diaryService: DiaryService) { }

  async ngOnInit() {
    try {
      this.diarys = await this.diaryService.getDiarys();
    } catch (e) {
      console.log(e);
    }
  }

  public numOfDiarys(thediarys) {
    return thediarys.length;
  }

}
