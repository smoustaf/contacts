import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DiaryListComponent } from './diary-list/diary-list.component';
import { DiaryEditComponent } from './diary-edit/diary-edit.component';
import { MainPageComponent } from './main-page/main-page.component';
import { DiaryFormComponent } from './diary-form/diary-form.component';
import { RoutingModule } from './routing/routing.module';
import { AboutComponent } from './about/about.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';





@NgModule({
  declarations: [
    AppComponent,
    DiaryListComponent,
    DiaryEditComponent,
    MainPageComponent,
    DiaryFormComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
